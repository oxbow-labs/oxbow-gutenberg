<?php

namespace Drupal\oxbow_gutenberg\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Class OxbowThemeNegotiator.
 *
 * @package Drupal\oxbow_gutenberg
 */
class OxbowThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    // node add forms
    $node_type = $route_match->getParameter('node_type');
    if (is_object($node_type)) {
      $node_type = $node_type->id();
    }
    if (
      !empty($node_type)
      && in_array($node_type, ['page'])
      && $route_name == 'node.add'
    ) {
      return TRUE;
    }
    // node edit forms
    $node = $route_match->getParameter('node');
    if (!empty($node) && is_string($node)) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node);
    }
    if (
      !empty($node)
      && in_array($node->bundle(), ['page'])
      && $route_name == 'entity.node.edit_form'
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return 'starter';
  }

}
